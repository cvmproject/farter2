function ENT:SetupDataTable()
    self:AddDataTableEntry("PointingVector", "Vector", nil)
end


function ENT:Spawn()
    local pointing_vector = self:GetPointingVector()
    sound.PlaySound("rig"..math.random(3), {entity_id=self:GetID()})
    if CLIENT then
        for i = 1, 8 do
            local shitticle = ents.Create("shitticle")
            shitticle:SetPos(self:GetPos())
            shitticle.color = Color(255, 255, 0)
            shitticle:SetVel(self:GetVel()+pointing_vector*2.5 + Vector(math.random()*1.25-0.725, math.random()*1.25-0.725))
            ents.SpawnClientside(shitticle)
        end
        return
    end
    self:Remove()
end
