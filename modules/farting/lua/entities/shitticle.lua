local SHIT_COLOR = Color(80, 43, 24)

function ENT:Init()
    self.size = 0.25
    self.size_decay = 0.5
    self.color = SHIT_COLOR
end

function ENT:Draw()
    local pos = self:GetPos()
    draw.SetColor(self.color)
    draw.Rect(pos.x - self.size/2, pos.y - self.size/2, self.size, self.size)
end

function ENT:Tick(delta)
    self.size = self.size - delta*self.size_decay
    if self.size <= 0 then
        self:Remove()
    end
    self:SetPos(self:GetPos() + self:GetVel()*delta)
end
